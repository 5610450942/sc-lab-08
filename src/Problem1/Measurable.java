package Problem1;

public interface Measurable {
	public double getMeasure();
}

package Problem1;


public class Product implements Taxable{
	private String name;
	private double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	
	public double getTax() {
		return price*0.07;
	}
	
	public String toString(){
		return name+" price: "+price;
	}


}

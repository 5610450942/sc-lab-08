package Problem1;

public interface Taxable {
	double getTax();
}
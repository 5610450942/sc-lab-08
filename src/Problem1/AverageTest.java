package Problem1;

import java.util.ArrayList;


public class AverageTest {

	public static void main(String[] args) {
		setTestCase();
		System.out.println("----------------------------------------------------------------");
		setTestMin();
		System.out.println("----------------------------------------------------------------");
		setTestTax();
	}
	
	private static void setTestCase(){
		Measurable[] objects = new Measurable[3];
		objects[0] = new Person("Thanawan", 169,1000);
		objects[1] = new Person("Nattapol", 167,2000);
		objects[2] = new Person("Nattachai", 165,3000);	
		System.out.println(objects[0]+"\n"+objects[1]+"\n"+objects[2]);
		System.out.println(Data.average(objects));
	}
	public static void setTestMin(){
		Measurable p1 = new Person("Thanawan", 169,1000);
		Measurable p2 = new Person("Nattapol", 167,2000);	
		System.out.println(p1+"\n"+p2);
		System.out.println("Min: "+Data.min(p1, p2));
		
		Measurable c1 = new Country("Thai", 555);
		Measurable c2 = new Country("USA", 1000);
		System.out.println(c1+"\n"+c2);
		System.out.println("Min: "+Data.min(c1, c2));
		
		Measurable b1 = new BankAccount("Thanawan",50);
		Measurable b2 = new BankAccount("Nattapol",200);
		System.out.println(b1+"\n"+b2);
		System.out.println("Min: "+Data.min(b1, b2));

	}
	public static void setTestTax(){
		//person
				System.out.println("<< Person Test >>");
				ArrayList<Taxable> persons = new ArrayList<Taxable>();
				persons.add(new Person("Thanawan",167, 100000));
				persons.add(new Person("Natapol",157, 500000));
				System.out.println(persons.get(0).toString()+" tax: "+persons.get(0).getTax());
				System.out.println(persons.get(1).toString()+" tax: "+persons.get(1).getTax());
				System.out.println("sum: "+TaxCalculator.sum(persons));
				//company
				System.out.println("<< Company Test >>");
				ArrayList<Taxable> companys = new ArrayList<Taxable>();
				companys.add(new Company("AIS", 1000000,800000));
				companys.add(new Company("TRUE", 2000000,1000000));
				System.out.println(companys.get(0).toString()+" tax: "+companys.get(0).getTax());
				System.out.println(companys.get(1).toString()+" tax: "+companys.get(1).getTax());
				System.out.println("sum: "+TaxCalculator.sum(companys));
				//product
				System.out.println("<< Product Test >>");
				ArrayList<Taxable> products = new ArrayList<Taxable>();
				products.add(new Product("milk", 100));
				products.add(new Product("eggs", 200));
				System.out.println(products.get(0).toString()+" tax: "+products.get(0).getTax());
				System.out.println(products.get(1).toString()+" tax: "+products.get(1).getTax());
				System.out.println("sum: "+TaxCalculator.sum(products));
				//measurable
				System.out.println("<< All Test >>");
				ArrayList<Taxable> measureable = new ArrayList<Taxable>();
				measureable.add(new Person("Thanawan",170, 100000));
				measureable.add(new Company("AIS", 1000000,800000));
				measureable.add(new Product("milk", 100));
				System.out.println(persons.get(0).toString()+" tax: "+persons.get(0).getTax());
				System.out.println(companys.get(0).toString()+" tax: "+companys.get(0).getTax());
				System.out.println(products.get(0).toString()+" tax: "+products.get(0).getTax());
				System.out.println("sum: "+TaxCalculator.sum(measureable));
			}

}
